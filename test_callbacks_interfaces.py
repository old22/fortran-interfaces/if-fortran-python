import callbacks_interfaces
import sys
EPS_F64 = sys.float_info.epsilon


def f(x):
    return x + 1e0


def g(x):
    return 1e0 / x


def gf(x):
    return g(f(x))


if __name__ == "__main__":
    print(dir(callbacks_interfaces))
    # assert "mod_factorial" in factorial
    for x_i in range(0, 20):
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            callbacks_interfaces.callbacks_interfaces.conv(f, g, x_i),
            gf(x_i)
        ))
        assert(abs(callbacks_interfaces.callbacks_interfaces.conv(f, g, x_i) - gf(x_i)) < EPS_F64)
        pass
    pass
