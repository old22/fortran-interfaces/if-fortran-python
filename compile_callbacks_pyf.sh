#!/bin/bash
# Activate Python environment.
source .venv/bin/activate;
# Compile the MODIFIED callbacks.pyf with support to external function callbacks and the source code.
python -m numpy.f2py -c --fcompiler=gfortran src/callbacks.f90 callbacks.pyf;
# python -m numpy.f2py --latex-doc src/factorial.f90 -m factorial;