# Interfacing Fortran to Python using F2PY

Numpy includes the F2PY program for compiling Fortran
code and building the required Python interfaces for
easy Fortran function/subroutine calls as well as
Python function callings within Fortran code. However,
F2PY support for F90/F95 is rather lackluster,
particularly on generating abstract interfaces
commonly used on the 90/95 standards.

This guide serves as an step-by-step tutorial for
handling abstract interfaces on callback functions
or subroutines provided as function or subroutine
arguments, for example:

```fortran
module callbacks_interfaces
    ! Callback functions as function arguments.
    implicit none

    abstract interface
        ! Input function specification.
        function f_r_r(x) result(f)
            real(kind=8), intent(in) :: x
            real(kind=8) :: f
        end function f_r_r
    end interface
contains

    ! Testing function.
    function conv(f, g, x) result(out)
        real(kind=8), intent(in) :: x
        real(kind=8) :: out
        ! Input functions.
        procedure(f_r_r) :: f
        procedure(f_r_r) :: g

        out = g(f(x))
    end function conv

end module callbacks_interfaces
```

Note that even if this information can be derived
from F2PY [documentation](https://numpy.org/devdocs/f2py/python-usage.html#call-back-arguments),
the test example consists of a module-less
F77 subroutine. The information below will try to
use modern and real-like examples (I suppose?).

## Example 1: Simple Fortran function call with real/integer arguments.

In order to introduce F2PY, a quick factorial example is
provided as introduction.

The source code is located at ``src/factorial.f90`` and
implements the (recursive) factorial function:

```fortran
module mod_factorial
    ! Test module. Implements simple factorial function.
    implicit none

    contains

    pure recursive function factorial(n) result(f)
        ! Factorial(n). Note! negative values return value 1.
        integer(kind=8), intent(in) :: n  ! Input value.
        integer(kind=8) :: f
        if(n <= 1) then
            f = 1
        else
            f = n * factorial(n-1)
        end if
    end function factorial
end module mod_factorial
```

This code can be interfaced without manual additions,
so it can be built with straight forward. In this case,
I am using the `gfortran` compiler, a custom build
directory `_build`, an interfaced module name `factorial`
and the source code `src/factorial.f90`:

```bash
$ python -m numpy.f2py -c --fcompiler=gfortran --build-dir _build -m factorial src/factorial.f90
```

Done! the dynamic library `factorial.cpython-36m-x86_64-linux-gnu.so`
(or something equivalent) is built in the root directory.
A testing Python script `test_factorial.py` guaranties
the function call success:

```python
import factorial
from math import factorial as fact_python

if __name__ == "__main__":
    print(dir(factorial))
    # assert "mod_factorial" in factorial
    for n in range(0, 20):
        print("({})! = {} (Fortran), {} (Python)".format(
            n,
            factorial.mod_factorial.factorial(n),
            fact_python(n)
        ))
        assert(factorial.mod_factorial.factorial(n) == fact_python(n))
        pass
    pass
```

The result verifies that the factorial call
was successful:

```raw
(0)! = 1 (Fortran), 1 (Python)
(1)! = 1 (Fortran), 1 (Python)
(2)! = 2 (Fortran), 2 (Python)
(3)! = 6 (Fortran), 6 (Python)
(4)! = 24 (Fortran), 24 (Python)
(5)! = 120 (Fortran), 120 (Python)
(6)! = 720 (Fortran), 720 (Python)
(7)! = 5040 (Fortran), 5040 (Python)
(8)! = 40320 (Fortran), 40320 (Python)
(9)! = 362880 (Fortran), 362880 (Python)
(10)! = 3628800 (Fortran), 3628800 (Python)
(11)! = 39916800 (Fortran), 39916800 (Python)
(12)! = 479001600 (Fortran), 479001600 (Python)
(13)! = 6227020800 (Fortran), 6227020800 (Python)
(14)! = 87178291200 (Fortran), 87178291200 (Python)
(15)! = 1307674368000 (Fortran), 1307674368000 (Python)
(16)! = 20922789888000 (Fortran), 20922789888000 (Python)
(17)! = 355687428096000 (Fortran), 355687428096000 (Python)
(18)! = 6402373705728000 (Fortran), 6402373705728000 (Python)
(19)! = 121645100408832000 (Fortran), 121645100408832000 (Python)
```

## Example 2: Simple callback function arguments.

A first contact with function arguments is introduced
in ``src/callbacks.f90`` with the function ``conv``,
which returns the function composition of input
functions ``f`` and ``g`` at value ``x``:

```fortran
! Callback function outside module.

function conv(f, g, x) result(out)
    real(kind=8), intent(in) :: x
    real(kind=8) :: out
    external f
    external g
    real(kind=8) :: f, g

    out = g(f(x))
end function conv
```

First, the PYF code signature file must be generated.
The PYF file will guide F2PY into correctly compiling
Fortran code and fix the interface issues. The compiler
options and execution code are as follows:

* ``--overwrite-signature``: override existing PYF files.
* ``--build-dir BUILD_DIR``: build directory AND PYF output directory.
* ``-m MODULE``: interfacing Python module name.
* ``-h PYF_FILE``: output PYF file name.

```bash
$ python -m numpy.f2py --overwrite-signature --build-dir _build -m callbacks src/callbacks.f90 -h callbacks.pyf
```

The generated raw file, ``_build/callbacks.pyf`` is as
follows:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface 
        function g(f_x_) result (out) ! in :callbacks:src/callbacks.f90:conv:unknown_interface
            real :: f_x_
            real(kind=8) :: out
        end function g
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks ! in 
    interface  ! in :callbacks
        function conv(f,g,x) result (out) ! in :callbacks:src/callbacks.f90
            use conv__user__routines
            external f
            external g
            real(kind=8) intent(in) :: x
            real(kind=8) :: out
        end function conv
    end interface 
end python module callbacks

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

The key aspect on this file is the ``*__user__routines``
(``conv__user__routines `` in this case) module. This
module includes the interfaces of all external functions
to be passed, which in this case should bethe externals
``f`` and ``g``. F2PY tried to do this by using the
code's function call line ``out = g(f(x))`` to no avail.
Using the following interface declaration solves it:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface
    !!! Edit function signature in order to correct parser errors.
        function f(x) result (out) ! in :callbacks:src/callbacks.f90:conv:unknown_interface
            real*8 intent(in) :: x
            real*8 :: out
        end function f
    !!! Add second function signature.
        function g(x) result (out) ! in :callbacks:src/callbacks.f90:conv:unknown_interface
            real*8 intent(in) :: x
            real*8 :: out
        end function g
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks ! in 
    interface  ! in :callbacks
        function conv(f,g,x) result (out) ! in :callbacks:src/callbacks.f90
            use conv__user__routines
            external f
            external g
            real*8 intent(in) :: x
            real*8 :: out
        end function conv
    end interface 
end python module callbacks

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Due to the interface function naming respecting the
callback dummy arguments naming, no more actions are
needed, though this actions will be explained in
future examples. The new PYF file is moved to the
root directory in order to compile the interface Python
module and avoid being replaced:

```bash
$ python -m numpy.f2py -c --fcompiler=gfortran src/callbacks.f90 callbacks.pyf
```

Done! the dynamic library `callbacks.cpython-36m-x86_64-linux-gnu.so`
(again, it could be any other name) is built in the
root directory. A new testing Python script
`test_callbacks.py` is made in where the ``f`` and
``g`` functions are implemented and called:

```python
"""Test Fortran ``callback`` module call on Python
using F2PY.
"""
import callbacks
import sys
EPS = sys.float_info.epsilon


def f(x):
    """F function."""
    return x + 1


def g(x):
    """G function."""
    return 1e0 / x


def gf(x):
    """Composition function g(f(x))."""
    return g(f(x))


if __name__ == "__main__":
    print(dir(callbacks))
    # assert "mod_factorial" in factorial
    for x_i in range(0, 20):
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            callbacks.conv(f, g, x_i),
            gf(x_i)
        ))
        assert(abs(callbacks.conv(f, g, x_i) - gf(x_i)) < EPS)
        pass
    pass
```

The result verifies that ``conv`` works as intended:

```raw
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_callbacks_error', 'conv']
g(f( 0)) = 1.000  (Fortran), 1.000  (Python)
g(f( 1)) = 0.500  (Fortran), 0.500  (Python)
g(f( 2)) = 0.333  (Fortran), 0.333  (Python)
g(f( 3)) = 0.250  (Fortran), 0.250  (Python)
g(f( 4)) = 0.200  (Fortran), 0.200  (Python)
g(f( 5)) = 0.167  (Fortran), 0.167  (Python)
g(f( 6)) = 0.143  (Fortran), 0.143  (Python)
g(f( 7)) = 0.125  (Fortran), 0.125  (Python)
g(f( 8)) = 0.111  (Fortran), 0.111  (Python)
g(f( 9)) = 0.100  (Fortran), 0.100  (Python)
g(f(10)) = 0.091  (Fortran), 0.091  (Python)
g(f(11)) = 0.083  (Fortran), 0.083  (Python)
g(f(12)) = 0.077  (Fortran), 0.077  (Python)
g(f(13)) = 0.071  (Fortran), 0.071  (Python)
g(f(14)) = 0.067  (Fortran), 0.067  (Python)
g(f(15)) = 0.062  (Fortran), 0.062  (Python)
g(f(16)) = 0.059  (Fortran), 0.059  (Python)
g(f(17)) = 0.056  (Fortran), 0.056  (Python)
g(f(18)) = 0.053  (Fortran), 0.053  (Python)
g(f(19)) = 0.050  (Fortran), 0.050  (Python)
```

## Example 3: Callback function arguments on a module.

Instead of having a dangling function in the middle of
a file, the following example wraps the previous
function inside a module. The source code
``src/callbacks_mod.f95`` is as follows:

```fortran
module callbacks_simple
! Callback function outside module.
implicit none
    contains

    function conv(f, g, x) result(out)
        real(kind=8), intent(in) :: x
        real(kind=8) :: out
        external f
        external g
        real(kind=8) :: f, g

        out = g(f(x))
    end function conv

end module callbacks_simple
```

Similar to _Example 2_, the PYF signature file
has to be created. In order to do so, the
``build_callbacks_mod_pyf.sh`` bash script was
created:

```bash
#!/bin/bash
NAME="callbacks_mod"
FLAGS="--overwrite-signature --build-dir _build"
SOURCES="src/callbacks_mod.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the source code signature callbacks.pyf on "_build" dir. Output file must be later edited.
python -m numpy.f2py $FLAGS -m "$NAME" $SOURCES -h "$NAME".pyf;
```

In case of not using a Python virtual environment (or
using another name), delete or edit the _source ..._
line.

The generated PYF file, ``_build/callbacks_mod.pyf`` is as
follows:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface 
        function g(f_x_) result (out) ! in :callbacks_mod:src/callbacks_mod.f95:callbacks_simple:conv:unknown_interface
            real :: f_x_
            real(kind=8) :: out
        end function g
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks_mod ! in 
    interface  ! in :callbacks_mod
        module callbacks_simple ! in :callbacks_mod:src/callbacks_mod.f95
            function conv(f,g,x) result (out) ! in :callbacks_mod:src/callbacks_mod.f95:callbacks_simple
                use conv__user__routines
                external f
                external g
                real(kind=8) intent(in) :: x
                real(kind=8) :: out
            end function conv
        end module callbacks_simple
    end interface 
end python module callbacks_mod

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Same as _Example 2_, F2PY generates a
``conv__user__routines`` module that has to be
edited. In addition, the ``conv`` function is
wrapped by two python modules now: the Python
interface module and the Fortran module.

The new PYF file, changed same as in _Example 2_
and placed at the root directory, is as follows:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface 
        function f(x) result (out)
            ! NOTE! kind=8 required on both.
            real(kind=8) :: x
            real(kind=8) :: out
        end function f
        function g(x) result (out)
            ! NOTE! kind=8 required on both.
            real(kind=8) :: x
            real(kind=8) :: out
        end function g
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks_mod ! in 
    interface  ! in :callbacks_mod
        module callbacks_simple ! in :callbacks_mod:src/callbacks_mod.f95
            function conv(f,g,x) result (out) ! in :callbacks_mod:src/callbacks_mod.f95:callbacks_simple
                use conv__user__routines, f=>f, g=>g
                external f
                external g
                real(kind=8) intent(in) :: x
                real(kind=8) :: out
            end function conv
        end module callbacks_simple
    end interface 
end python module callbacks_mod

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Note that now the ``conv`` function not only
makes use of the ``conv__user__routines`` module,
it explicitly links dummy arguments ``f`` and ``g``
to its interfaces. As dummy and interface share same
name it is not required but useful for later
purposes.

The Python module is compiled using the ``compile_callbacks_mod_pyf.sh``
bash script:

```bash
#!/bin/bash
NAME="callbacks_mod"
FLAGS="--fcompiler=gfortran"
SOURCES="src/callbacks_mod.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the MODIFIED callbacks.pyf with support to external function callbacks and the source code.
python -m numpy.f2py -c $FLAGS $SOURCES "$NAME".pyf;
```

Done! the dynamic library `callbacks_mod.cpython-36m-x86_64-linux-gnu.so`
is built in the root directory. The Python testing
script is similar to the previous case, though
now requires and additional module call:

```python
"""Test Fortran ``callback_simple`` module call on Python
using F2PY.
"""
import callbacks_mod
import sys
EPS = sys.float_info.epsilon


def f(x):
    """F function."""
    return x + 1


def g(x):
    """G function."""
    return 1e0 / x


def gf(x):
    """Composition function g(f(x))."""
    return g(f(x))


if __name__ == "__main__":
    print(dir(callbacks_mod))
    # assert "mod_factorial" in factorial
    for x_i in range(0, 20):
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            callbacks_mod.callbacks_simple.conv(f, g, x_i),
            gf(x_i)
        ))
        assert(abs(callbacks_mod.callbacks_simple.conv(f, g, x_i) - gf(x_i)) < EPS)
        pass
    pass
```

The result verifies that ``conv`` works as it should:

```raw
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_callbacks_mod_error', 'callbacks_simple']
g(f( 0)) = 1.000  (Fortran), 1.000  (Python)
g(f( 1)) = 0.500  (Fortran), 0.500  (Python)
g(f( 2)) = 0.333  (Fortran), 0.333  (Python)
g(f( 3)) = 0.250  (Fortran), 0.250  (Python)
g(f( 4)) = 0.200  (Fortran), 0.200  (Python)
g(f( 5)) = 0.167  (Fortran), 0.167  (Python)
g(f( 6)) = 0.143  (Fortran), 0.143  (Python)
g(f( 7)) = 0.125  (Fortran), 0.125  (Python)
g(f( 8)) = 0.111  (Fortran), 0.111  (Python)
g(f( 9)) = 0.100  (Fortran), 0.100  (Python)
g(f(10)) = 0.091  (Fortran), 0.091  (Python)
g(f(11)) = 0.083  (Fortran), 0.083  (Python)
g(f(12)) = 0.077  (Fortran), 0.077  (Python)
g(f(13)) = 0.071  (Fortran), 0.071  (Python)
g(f(14)) = 0.067  (Fortran), 0.067  (Python)
g(f(15)) = 0.062  (Fortran), 0.062  (Python)
g(f(16)) = 0.059  (Fortran), 0.059  (Python)
g(f(17)) = 0.056  (Fortran), 0.056  (Python)
g(f(18)) = 0.053  (Fortran), 0.053  (Python)
g(f(19)) = 0.050  (Fortran), 0.050  (Python)
```

## Example 4: Interfaced callback function arguments.

Finally, we can get rid of the ``external`` keyword
and use an actual ``abstract interface``. The source
code used in this example is located at
``src/callback_interfaces.f95`` and looks as follow:

```fortran
module callbacks_interfaces
    ! Callback function outside module.
    implicit none

    abstract interface
        function f_r_r(x) result(f)
            real(kind=8), intent(in) :: x
            real(kind=8) :: f
        end function f_r_r
    end interface
    
contains

    function conv(f, g, x) result(out)
        real(kind=8), intent(in) :: x
        real(kind=8) :: out
        procedure(f_r_r) :: f
        procedure(f_r_r) :: g

        out = g(f(x))
    end function conv

end module callbacks_interfaces
```

Similar to _Example 3_, ``build_callbacks_interfaces_pyf.sh``
bash script for generating the PYF example:

```bash
#!/bin/bash
NAME="callbacks_interfaces"
FLAGS="--overwrite-signature --build-dir _build"
SOURCES="src/callbacks_interfaces.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the source code signature callbacks.pyf on "_build" dir. Output file must be later edited.
python -m numpy.f2py $FLAGS -m "$NAME" $SOURCES -h "$NAME".pyf;
```

The generated raw PYF file, ``_build/callbacks_interfaces.pyf``
is as follows:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module callbacks_interfaces ! in 
    interface  ! in :callbacks_interfaces
        module callbacks_interfaces ! in :callbacks_interfaces:src/callbacks_interfaces.f95
            function f_r_r(x) result (f) ! in :callbacks_interfaces:src/callbacks_interfaces.f95:callbacks_interfaces
                real(kind=8) intent(in) :: x
                real(kind=8) :: f
            end function f_r_r
        end module callbacks_interfaces
        function conv(f,g,x) result (out) ! in :callbacks_interfaces:src/callbacks_interfaces.f95
            real :: f
            real :: g
            real(kind=8) intent(in) :: x
            real(kind=8) :: out
        end function conv
    end interface 
end python module callbacks_interfaces

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Well, at first glance it looks legit: function interface
detected and real ``f`` and ``g`` callback functions.
However, it can be seen that the ``f_r_r`` function
is treated as a properly declared and implemented
function rather than an abstract interface, leading
the compiler into thinking that ``f`` (and ``g``) ARE
``f_r_r`` and returning an error stating that said
function does not exist.


In order to correct this, the following tasks are done:
1. Create a Python module named ``__user__routines``.
2. Create the ``__user__interface`` interface inside
   said module.
3. CUT the abstract interface function/subroutine
   declarations and place them at the ``__user__interface``.
4. Inside the ``conv`` function, load the interface
   declaration by placing a ``use __user__routines``
   and binding the dummy arguments to its correct
   function/subroutine interface by typing, at the end
   of the ``use`` line, a ``DUMMY=>INTERFACE`` binder.
5. Mark ``f`` and ``g`` as ``external``.
5. Optionally, in order to prevail the module structure,
   place the ``conv`` function declaration inside
   the ``callbacks_interfaces`` module.

The resulting file looks like this:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module __user__routines
    interface _user_interface
        function ff(x) result (out)
            real(kind=8) :: x
            real(kind=8) :: out
        end function ff
        function gg(x) result (out)
            real(kind=8) :: x
            real(kind=8) :: out
        end function gg
    end interface _user_interface
end python module __user__routines
python module callbacks_interfaces ! in 
    interface  ! in :callbacks_interfaces
    module callbacks_interfaces
        function conv(f,g,x) result (out) ! in :callbacks_interfaces:src/callbacks_interfaces.f95
            ! NOTE! always use __user_routines for function callbacks.
            use __user__routines, f=>ff, g=>gg
            ! NOTE! f and g always external.
            external f
            external g
            real(kind=8) intent(in) :: x
            real(kind=8) :: out
        end function conv
        end module callbacks_interfaces
    end interface 
end python module callbacks_interfaces

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Compiling with ``compile_callbacks_interfaces_pyf.sh``
bash script:

```bash
#!/bin/bash
NAME="callbacks_interfaces"
FLAGS="--fcompiler=gfortran"
SOURCES="src/callbacks_interfaces.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the MODIFIED callbacks.pyf with support to external function callbacks and the source code.
python -m numpy.f2py -c $FLAGS $SOURCES "$NAME".pyf;
```

Done! the dynamic library `callbacks_interfaces.cpython-36m-x86_64-linux-gnu.so`
is built at the root directory. Testing it with the
``test_callbacks_interfaces.py`` Python script...

```python
import callbacks_interfaces
import sys
EPS_F64 = sys.float_info.epsilon


def f(x):
    return x + 1e0


def g(x):
    return 1e0 / x


def gf(x):
    return g(f(x))


if __name__ == "__main__":
    print(dir(callbacks_interfaces))
    # assert "mod_factorial" in factorial
    for x_i in range(0, 20):
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            callbacks_interfaces.callbacks_interfaces.conv(f, g, x_i),
            gf(x_i)
        ))
        assert(abs(callbacks_interfaces.callbacks_interfaces.conv(f, g, x_i) - gf(x_i)) < EPS_F64)
        pass
    pass
```

... yields:

```raw
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_callbacks_interfaces_error', 'callbacks_interfaces']
g(f( 0)) = 1.000  (Fortran), 1.000  (Python)
g(f( 1)) = 0.500  (Fortran), 0.500  (Python)
g(f( 2)) = 0.333  (Fortran), 0.333  (Python)
g(f( 3)) = 0.250  (Fortran), 0.250  (Python)
g(f( 4)) = 0.200  (Fortran), 0.200  (Python)
g(f( 5)) = 0.167  (Fortran), 0.167  (Python)
g(f( 6)) = 0.143  (Fortran), 0.143  (Python)
g(f( 7)) = 0.125  (Fortran), 0.125  (Python)
g(f( 8)) = 0.111  (Fortran), 0.111  (Python)
g(f( 9)) = 0.100  (Fortran), 0.100  (Python)
g(f(10)) = 0.091  (Fortran), 0.091  (Python)
g(f(11)) = 0.083  (Fortran), 0.083  (Python)
g(f(12)) = 0.077  (Fortran), 0.077  (Python)
g(f(13)) = 0.071  (Fortran), 0.071  (Python)
g(f(14)) = 0.067  (Fortran), 0.067  (Python)
g(f(15)) = 0.062  (Fortran), 0.062  (Python)
g(f(16)) = 0.059  (Fortran), 0.059  (Python)
g(f(17)) = 0.056  (Fortran), 0.056  (Python)
g(f(18)) = 0.053  (Fortran), 0.053  (Python)
g(f(19)) = 0.050  (Fortran), 0.050  (Python)
```

## Example 5: Separated interface declaration and calling function implementation.

This example is similar to the previous one,
but now we will be using a subroutine and two
source files for separating the interface
declarations. Source files are...

...``src/interfaces.f95``...

```fortran
module interfaces
    implicit none

    abstract interface
        function if_f(x) result(f)
            real(kind=8), intent(in) :: x
            real(kind=8) :: f
        end function if_f

        subroutine if_s(x_in, x_out)
            real(kind=8), intent(in) :: x_in
            real(kind=8), intent(inout) :: x_out
        end subroutine if_s
    end interface
end module interfaces
```

... and ``src/callbacks_complete.f95``.

```fortran
module callbacks_complete
    use interfaces, only: if_f, if_s
    implicit none

    contains

        function conv(f, g, x) result(out)
            real(kind=8), intent(in) :: x
            procedure(if_f) :: f
            procedure(if_s) :: g
            real(kind=8) :: out

            call g(f(x), out)
        end function conv
end module callbacks_complete
```

Create PYF file with ``build_callbacks_complete_pyf.sh``:

```bash
#!/bin/bash
NAME="callbacks_complete"
FLAGS="--overwrite-signature --build-dir _build"
SOURCES="src/interfaces.f95 src/callbacks_complete.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the source code signature callbacks.pyf on "_build" dir. Output file must be later edited.
python -m numpy.f2py $FLAGS -m "$NAME" $SOURCES -h "$NAME".pyf;
```

The generated raw PYF file, ``_build/callbacks_complete.pyf``
:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface 
        subroutine g(e_f_x_err,out) ! in :callbacks_complete:src/callbacks_complete.f95:conv:unknown_interface
            real :: e_f_x_err
            real(kind=8) :: out
        end subroutine g
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks_complete ! in 
    interface  ! in :callbacks_complete
        module interfaces ! in :callbacks_complete:src/interfaces.f95
            function if_f(x) result (f) ! in :callbacks_complete:src/interfaces.f95:interfaces
                real(kind=8) intent(in) :: x
                real(kind=8) :: f
            end function if_f
            subroutine if_s(x_in,x_out) ! in :callbacks_complete:src/interfaces.f95:interfaces
                real(kind=8) intent(in) :: x_in
                real(kind=8) intent(inout) :: x_out
            end subroutine if_s
        end module interfaces
    end interface 
    module callbacks_complete ! in :callbacks_complete
        use interfaces, only: if_f,if_s
        function conv(f,g,x) result (out) ! in :callbacks_complete:src/callbacks_complete.f95
            use conv__user__routines
            real :: f
            external g
            real(kind=8) intent(in) :: x
            real(kind=8) :: out
        end function conv
    end module callbacks_complete
end python module callbacks_complete

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

All source file modules, functions and interfaces
are placed at the same PYF file. We just need to:

1. override the ``*__user_interface`` functions and
subroutines with the ones at the actual interface
module,
2. delete said interface module,
3. mark ``f`` and ``g`` as external,
4. place the ``use conv__user__routines`` command
   below the ``conv`` function and
5. Bind dummy arguments to interface function/subroutine. 

The end result (omit ! comments) is:

```fortran
!    -*- f90 -*-
! Note: the context of this file is case sensitive.

python module conv__user__routines 
    interface conv_user_interface 
        function if_f(x) result (f) ! in :callbacks_complete:src/interfaces.f95:interfaces
            real(kind=8) intent(in) :: x
            real(kind=8) :: f
        end function if_f
        subroutine if_s(x_in,x_out) ! in :callbacks_complete:src/interfaces.f95:interfaces
            real(kind=8) intent(in) :: x_in
            real(kind=8) intent(inout) :: x_out
        end subroutine if_s
    end interface conv_user_interface
end python module conv__user__routines
python module callbacks_complete ! in
    ! NOTE! F2PY treats abstract interfaces as real functions, copy-paste them into *__user_routines.
    !interface  ! in :callbacks_complete
    !    module interfaces ! in :callbacks_complete:src/interfaces.f95
    !        function if_f(x) result (f) ! in :callbacks_complete:src/interfaces.f95:interfaces
    !            real intent(in) :: x
    !            real :: f
    !        end function if_f
    !        subroutine if_s(x_in,x_out) ! in :callbacks_complete:src/interfaces.f95:interfaces
    !            real intent(in) :: x_in
    !            real intent(out) :: x_out
    !        end subroutine if_s
    !    end module interfaces
    !end interface
    module callbacks_complete ! in :callbacks_complete
        use interfaces, only: if_f,if_s
        function conv(f,g,x) result (out) ! in :callbacks_complete:src/callbacks_complete.f95
            use conv__user__routines, f=>if_f, g=>if_s
            external f
            external g
            ! intent(callback, hide) g
            real(kind=8) intent(in) :: x
            real(kind=8) :: out
        end function conv
    end module callbacks_complete
end python module callbacks_complete

! This file was auto-generated with f2py (version:2).
! See http://cens.ioc.ee/projects/f2py2e/
```

Compiling with ``compile_callbacks_complete_pyf.sh``
bash script:

```bash
#!/bin/bash
NAME="callbacks_complete"
FLAGS="--fcompiler=gfortran"
SOURCES="src/interfaces.f95 src/callbacks_complete.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the MODIFIED callbacks.pyf with support to external function callbacks and the source code.
python -m numpy.f2py -c $FLAGS $SOURCES "$NAME".pyf;
```

Once the dynamic library `callbacks_complete.cpython-36m-x86_64-linux-gnu.so`
is built, we test it with the ``test_callbacks_complete.py``
Python script.

Note that ``g`` was defined as a
subroutine with its second argument as the (in)out
argument and thus the Python ``g`` function
implementation's second argument MUST BE MUTABLE
(list, dict, numpy array, etc.) and not an immutable
value (numbers, strings, etc.). The solution required
using numpy to calculate the reciprocal value of the
float ``x_in`` and store it on the ALREADY EXISTING
``x_out`` 0-dimensional numpy array. In addition,
using ``float`` was needed in order to print te
result on a formatted string. 

```python
import callbacks_complete
import numpy as np
import sys
EPS_F64 = sys.float_info.epsilon


def f(x: np.float64):
    return x + 1e0


def g(x_in: np.float64, x_out: np.ndarray):
    np.reciprocal(x_in, out=x_out)
    pass


def gf(x, xx):
    # xx = np.zeros(1, dtype=np.float64, order='F')
    g(f(x), xx)
    return xx


if __name__ == "__main__":
    print(dir(callbacks_complete))
    # assert "mod_factorial" in factorial
    x_f = np.zeros(1, dtype=np.float64, order='F')
    x_p = np.zeros(1, dtype=np.float64, order='F')
    for x_i in range(0, 20):
        x_f = callbacks_complete.callbacks_complete.conv(f, g, x_i)
        x_p = gf(x_i, x_p)
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            x_f,
            float(x_p))
        )
        assert(abs(x_f - x_p) < EPS_F64)
        pass
    pass
```

The script yielded the same:

```raw
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', '__version__', '_callbacks_complete_error', 'callbacks_complete']
g(f( 0)) = 1.000  (Fortran), 1.000  (Python)
g(f( 1)) = 0.500  (Fortran), 0.500  (Python)
g(f( 2)) = 0.333  (Fortran), 0.333  (Python)
g(f( 3)) = 0.250  (Fortran), 0.250  (Python)
g(f( 4)) = 0.200  (Fortran), 0.200  (Python)
g(f( 5)) = 0.167  (Fortran), 0.167  (Python)
g(f( 6)) = 0.143  (Fortran), 0.143  (Python)
g(f( 7)) = 0.125  (Fortran), 0.125  (Python)
g(f( 8)) = 0.111  (Fortran), 0.111  (Python)
g(f( 9)) = 0.100  (Fortran), 0.100  (Python)
g(f(10)) = 0.091  (Fortran), 0.091  (Python)
g(f(11)) = 0.083  (Fortran), 0.083  (Python)
g(f(12)) = 0.077  (Fortran), 0.077  (Python)
g(f(13)) = 0.071  (Fortran), 0.071  (Python)
g(f(14)) = 0.067  (Fortran), 0.067  (Python)
g(f(15)) = 0.062  (Fortran), 0.062  (Python)
g(f(16)) = 0.059  (Fortran), 0.059  (Python)
g(f(17)) = 0.056  (Fortran), 0.056  (Python)
g(f(18)) = 0.053  (Fortran), 0.053  (Python)
g(f(19)) = 0.050  (Fortran), 0.050  (Python)
```