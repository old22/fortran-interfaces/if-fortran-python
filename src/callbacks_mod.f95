module callbacks_simple
! Callback function outside module.
implicit none
    contains

    function conv(f, g, x) result(out)
        real(kind=8), intent(in) :: x
        real(kind=8) :: out
        external f
        external g
        real(kind=8) :: f, g

        out = g(f(x))
    end function conv

end module callbacks_simple