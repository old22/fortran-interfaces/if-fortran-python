#!/bin/bash
NAME="callbacks_complete"
FLAGS="--overwrite-signature --build-dir _build"
SOURCES="src/interfaces.f95 src/callbacks_complete.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the source code signature callbacks.pyf on "_build" dir. Output file must be later edited.
python -m numpy.f2py $FLAGS -m "$NAME" $SOURCES -h "$NAME".pyf;
# python -m numpy.f2py --latex-doc src/factorial.f90 -m factorial;