module callbacks_interfaces
    ! Callback function outside module.
    implicit none

    abstract interface
        function f_r_r(x) result(f)
            real(kind=8), intent(in) :: x
            real(kind=8) :: f
        end function f_r_r
    end interface

contains

    function conv(f, g, x) result(out)
        real(kind=8), intent(in) :: x
        real(kind=8) :: out
        procedure(f_r_r) :: f
        procedure(f_r_r) :: g

        out = g(f(x))
    end function conv

end module callbacks_interfaces