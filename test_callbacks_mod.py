"""Test Fortran ``callback_simple`` module call on Python
using F2PY.
"""
import callbacks_mod
import sys
EPS = sys.float_info.epsilon


def f(x):
    """F function."""
    return x + 1


def g(x):
    """G function."""
    return 1e0 / x


def gf(x):
    """Composition function g(f(x))."""
    return g(f(x))


if __name__ == "__main__":
    print(dir(callbacks_mod))
    # assert "mod_factorial" in factorial
    for x_i in range(0, 20):
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            callbacks_mod.callbacks_simple.conv(f, g, x_i),
            gf(x_i)
        ))
        assert(abs(callbacks_mod.callbacks_simple.conv(f, g, x_i) - gf(x_i)) < EPS)
        pass
    pass
