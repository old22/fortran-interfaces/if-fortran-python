module callbacks_complete
    use interfaces, only: if_f, if_s
    implicit none

    contains

        function conv(f, g, x) result(out)
            real(kind=8), intent(in) :: x
            procedure(if_f) :: f
            procedure(if_s) :: g
            real(kind=8) :: out

            call g(f(x), out)
        end function conv
end module callbacks_complete