import factorial
from math import factorial as fact_python

if __name__ == "__main__":
    print(dir(factorial))
    # assert "mod_factorial" in factorial
    for n in range(0, 20):
        print("({})! = {} (Fortran), {} (Python)".format(
            n,
            factorial.mod_factorial.factorial(n),
            fact_python(n)
        ))
        assert(factorial.mod_factorial.factorial(n) == fact_python(n))
        pass
    pass
