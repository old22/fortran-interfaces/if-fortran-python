#!/bin/bash
# Activate Python environment.
source .venv/bin/activate;
# Compile the source code signature callbacks.pyf on "_build" dir. Output file must be later edited.
python -m numpy.f2py --overwrite-signature --build-dir _build -m callbacks src/callbacks.f90 -h callbacks.pyf;
# python -m numpy.f2py --latex-doc src/factorial.f90 -m factorial;