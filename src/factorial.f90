module mod_factorial
    ! Test module. Implements simple factorial function.
    implicit none

    contains

    pure recursive function factorial(n) result(f)
        ! Factorial(n). Note! negative values return value 1.
        integer(kind=8), intent(in) :: n  ! Input value.
        integer(kind=8) :: f
        if(n <= 1) then
            f = 1
        else
            f = n * factorial(n-1)
        end if
    end function factorial
end module mod_factorial