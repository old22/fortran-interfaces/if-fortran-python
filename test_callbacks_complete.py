import callbacks_complete
import numpy as np
import sys
EPS_F64 = sys.float_info.epsilon


def f(x: np.float64):
    return x + 1e0


def g(x_in: np.float64, x_out: np.ndarray):
    np.reciprocal(x_in, out=x_out)
    pass


def gf(x, xx):
    # xx = np.zeros(1, dtype=np.float64, order='F')
    g(f(x), xx)
    return xx


if __name__ == "__main__":
    print(dir(callbacks_complete))
    # assert "mod_factorial" in factorial
    x_f = np.zeros(1, dtype=np.float64, order='F')
    x_p = np.zeros(1, dtype=np.float64, order='F')
    for x_i in range(0, 20):
        x_f = callbacks_complete.callbacks_complete.conv(f, g, x_i)
        x_p = gf(x_i, x_p)
        print("g(f({: >2d})) = {: <6.3f} (Fortran), {: <6.3f} (Python)".format(
            x_i,
            x_f,
            float(x_p))
        )
        assert(abs(x_f - x_p) < EPS_F64)
        pass
    pass
