#!/bin/bash
NAME="callbacks_complete"
FLAGS="--fcompiler=gfortran"
SOURCES="src/interfaces.f95 src/callbacks_complete.f95"

# Activate Python environment.
source .venv/bin/activate;
# Compile the MODIFIED callbacks.pyf with support to external function callbacks and the source code.
python -m numpy.f2py -c $FLAGS $SOURCES "$NAME".pyf;
# python -m numpy.f2py --latex-doc src/factorial.f90 -m factorial;